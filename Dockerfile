FROM ros:kinetic-ros-core
MAINTAINER Kai Waelti <Kai.Waelti@dfki.de>

RUN echo "Adding Dataspeed server to apt..." && \
    sh -c 'echo "deb [ arch=amd64 ] http://packages.dataspeedinc.com/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-dataspeed-public.list' && \
    apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 66F84AE1EB71A8AC108087DCAF677210FF6D3CDA && \
    apt-get update && apt-get install -q -y ros-kinetic-mobility-base build-essential && \
    echo "SDK install: Done"

WORKDIR /root/catkin_ws

COPY baxter-mobility-base-safety /root/catkin_ws/src

RUN rosdep install --from-paths src --rosdistro=kinetic -y

SHELL ["/bin/bash", "-c"]	# Change to bash shell for ros stuff

RUN source /opt/ros/kinetic/setup.bash && \
    cd /root/catkin_ws && \
    catkin_make -DCMAKE_BUILD_TYPE=Release

COPY launch-files /launch-files
COPY run-shells /run-shells

WORKDIR /root

COPY entrypoint.sh .

ENTRYPOINT ["/root/entrypoint.sh"]

ENV ROS_MASTER_URI "http://ros-master:11311"

CMD ["bash"]

