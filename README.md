# ROS Node for Mobility Base Safety

This node is responsible for human safety near the robot. Using a rectangular
footprint of the robot it calculates the distance to the nearest object and
adapts the speed adequately. If a object is too close the robot will not move.

## Using Container

Get image from RepoHub

```
docker pull repohub.enterpriselab.ch:5002/kawa/ros-kinetic-mb-safety:latest
```

or build yourself by cloning and running on the base of the robot itself

```
git clone https://gitlab.enterpriselab.ch/mt-kawa/ros-kinetic-mb-docker/ros-kinetic-mb-safety-docker.git
docker build -t ros-kinetic-mb-safety:latest .
```

and running 

```
docker run -it ros-kinetic-mb-safety bash
```

wait for container bash and run

```
roslaunch /launch-files/safety.launch
```

