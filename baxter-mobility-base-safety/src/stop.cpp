#include "ros/ros.h"
#include "std_msgs/String.h"
#include "sensor_msgs/LaserScan.h"
#include "mobility_base_core_msgs/SetMaxSpeed.h"


ros::Time last_time;
ros::Time last_print_time;

ros::ServiceClient client;

double min_distance = 0.0;

double REDUCE_DISTANCE = 2.0;
double STOP_DISTANCE = 1.0;
double max_speed_factor = 1.0;


void laserCallback(const sensor_msgs::LaserScan scan){
  ros::Time current_time;
  current_time = ros::Time::now();
      
    
        //laser information
        min_distance = 30.0;
	for(unsigned int i = 0; i < scan.ranges.size(); i++){
       		if (scan.ranges[i] < min_distance){
			min_distance = scan.ranges[i];
		}       		
     	}
    
        current_time = ros::Time::now();
   
        //reduce max_speed according to distance to obstacle
	if (min_distance < REDUCE_DISTANCE)	{
		if (min_distance < STOP_DISTANCE){
			max_speed_factor = 0.0;
		}else{
		max_speed_factor = - STOP_DISTANCE * (min_distance - REDUCE_DISTANCE) *
		(min_distance - REDUCE_DISTANCE) + 1;
		}	
	}else{
	max_speed_factor = 1.0;
	}	

	//call set_max_speed service
	mobility_base_core_msgs::SetMaxSpeed::Request req;
	mobility_base_core_msgs::SetMaxSpeed::Response resp;

	req.linear = max_speed_factor * 2.2 ;
	req.angular = 2.1;

	bool succes = client.call(req, resp);
	

        //print the message every 2 secs
        if ((current_time - last_print_time).toSec() > 2){
          ROS_INFO("Closest obstacle in %f m. Driving at %f * maximal speed", min_distance, max_speed_factor);
          last_print_time = ros::Time::now();
        }
   
        last_time = current_time;
//-----------------------------------------------------------------
}


int main(int argc, char* argv[]){
  ros::init(argc, argv, "safety_node");
  ros::NodeHandle n;
  last_time = ros::Time::now();

  client = n.serviceClient<mobility_base_core_msgs::SetMaxSpeed>("setMaxSpeed");
 

  ros::Subscriber sub = n.subscribe("/laser_birdcage_r2000/scan_filtered", 1000, laserCallback);

  ros::spin(); 
}
